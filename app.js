// Dependencias
const express = require('express');
const path = require('path');
const medicionesRoute = require('./routes/mediciones_route');
// Aliases para código más legible
const app = express();
const puerto = 7000;
// Leer JSON
app.use(express.json());
// Archivos estaticos
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/images', express.static(__dirname + 'public/images'))
// Usar API Rest
app.use('/api', medicionesRoute);
// Enviar la vista index como respuesta principal
app.get('/', (solicitud, respuesta) => {
    respuesta.sendFile(path.join(__dirname + '/views/index.html'))
})
// Enviar la vista dia si se recibe como parametro
app.get('/dia-*', (solicitud, respuesta) => {
  // const numDia = solicitud.params[0]
  respuesta.sendFile(path.join(__dirname + '/views/dia.html'))
})
// Página 404
app.use(function (solicitud, respuesta, next) {
  respuesta.status(404).send("Contenido no encontrado")
})
// Escuchar en el puerto 7000
app.listen(puerto, () => console.info(`App usando puerto ${puerto}`))