'use strict'

const express = require("express")
const medicionesController = require("../controllers/mediciones_controller")
const router = express.Router()

router.get('/mediciones&id=:id', medicionesController.obtenerMedicion)
router.get('/mediciones&dia=:dia', medicionesController.obtenerMedicionesDelDia)
router.get('/mediciones&desde=:desde&cantidad=:cantidad', medicionesController.obtenerMedicionesEntreIDs)
router.post('/mediciones', medicionesController.agregarMedicion)
router.put('/mediciones&id=:id', medicionesController.actualizarMedicion)
router.delete('/mediciones&id=:id', medicionesController.eliminarMedicion)
module.exports = router