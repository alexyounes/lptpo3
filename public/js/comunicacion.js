// Solicitar mediciones de un día
async function solicitarMedicionesDelDia(dia) {
    const solicitudMediciones = await fetch("http://localhost:7000/api/mediciones&dia=" + dia)
    const mediciones = await solicitudMediciones.json()
    return mediciones
}